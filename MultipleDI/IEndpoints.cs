﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultipleDI
{
    public interface IEndpoints
    {
        string ServiceName { get; }
        string Get();
    }
}
