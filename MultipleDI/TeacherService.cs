﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultipleDI
{
    public class TeacherService : IEndpoints
    {
        public string ServiceName => "TeacherService";

        public string Get()
        {
            return "TeacherService.Get()";
        }
    }
}
