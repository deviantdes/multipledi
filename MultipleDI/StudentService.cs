﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultipleDI
{
    public class StudentService : IEndpoints
    {
        public string ServiceName => "StudentService";

        public string Get()
        {
            return "StudentService.Get()";
        }
    }
}
