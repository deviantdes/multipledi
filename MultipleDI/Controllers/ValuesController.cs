﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MultipleDI.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IEnumerable<IEndpoints> _endpoints;

        public ValuesController(IEnumerable<IEndpoints> endpoints)
        {
            _endpoints = endpoints;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet("student")]
        public IActionResult GetStudent()
        {
            var student = _endpoints.First(x => x.ServiceName == "StudentService"); 
            return Ok(student.Get());
        }

        [HttpGet("teacher")]
        public IActionResult GetTeacher()
        {
            var student = _endpoints.First(x => x.ServiceName == "TeacherService");
            return Ok(student.Get());
        }

        [HttpGet("principal")]
        public IActionResult GetPrincipal()
        {
            var student = _endpoints.First(x => x.ServiceName == "PrincipleService");
            return Ok(student.Get());
        }
    }
}
