﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultipleDI
{
    public class PrincipalService : IEndpoints
    {
        public string ServiceName => "PrincipleService";

        public string Get()
        {
            return "PrincipleService.Get()";
        }
    }
}
